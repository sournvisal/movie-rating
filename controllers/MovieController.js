const Movie = require('../models/Movie');
const Rating = require('../models/Rating');
const passport = require('passport');

module.exports.controller = (app, _) => {
    app.get('/movies', (req, res) => {
        Movie.find({}, 'name description release_year genre', (err, movies) => {
            if (err) {
                console.log(err);
            }

            res.status(200).send({
                movies
            })
        });
    });

    app.get('/api/movies/:id', (req, res) => {
        Movie.findById(req.params.id, 'name description release_year genre', (err, movie) => {
            if (err) {
                console.log(err);
            }

            res.status(200).send(movie);
        });
    });

    app.post('/movies/rate/:id', (req, res) => {
        const rating = new Rating({
            movie_id: req.params.id,
            user_id: req.body.user_id,
            rate: req.body.rate,
            });

        rating.save(function(err, rating) {
            if (err) {
                console.log(err);
            }

            res.status(200).send(rating);
        });
    });
    
    app.post('/movies', (req, res) => {
        const body = _.pick(req.body, ['name', 'description', 'release_year', 'genre']);

        const newMovie = new Movie(body);

        newMovie.save((err, movie) => {
            if (err) {
                console.log(err);
            }

            res.status(200).send(movie);
        });
    });
};