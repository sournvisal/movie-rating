const User = require('../models/User');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const passportJWT = require('passport-jwt');
const jwt = require('jsonwebtoken');
const ExtractJwt = passportJWT.ExtractJwt;
const jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
jwtOptions.secretOrKey = 'thisisthesecretkey';

module.exports.controller = (app, _) => {

    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
    }, (email, password, done) => {
        User.getUserByEmail(email, (err, user) => {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            User.comparePassword(password, user.password, (error, isMatch) => {
                if (isMatch) {
                    return done(null, user);
                }
                return done(null, false);
            });
            return true;
        });
    }));
       

    app.post('/users/register', (req, res) => {
        const body = _.pick(req.body, ['name', 'email', 'password']);

        const newUser = new User(body);

        User.createUser(newUser, (err, user) => {
            if (err) {
                res.status(422).json({
                    message: 'Something went wrong, Please try again after some time!'
                });
            }

            res.status(200).send({user});
        });
    });

    app.post('/users/login',
        passport.authenticate('local', { failureRedirect: '/users/login' }),
        (req, res) => {
        res.redirect('/');
    });

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });
    
    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            done(err, user);
        });
    });
};

