const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('./config/Config');
const morgan = require('morgan');
const fs = require('fs');
const session = require('express-session');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const serveStatic = require('serve-static');
const history = require('connect-history-api-fallback');

const passportJWT = require('passport-jwt');
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;
const jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
jwtOptions.secretOrKey = 'movieratingapplicationsecretkey';

const app = express();
const router = express.Router();

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

app.use(session({
    secret: config.SECRET,
    resave: true,
    saveUninitialized: true,
    cookie: { httpOnly: false }
}));

app.use(passport.initialize());
app.use(passport.session());

mongoose.connect( config.DB, function () {
    console.log('connection has been made');
}).catch(err => {
    process.exit(1);
    process.on('exit', (code) => {
        setTimeout(() => {
            console.log('This will not run, there are sth wrong with connection to db');
            console.error(err.stack);
        }, 0);
    });
});

fs.readdirSync("controllers").forEach(function (file) {
    if (file.substr(-3) == ".js") {
        const route = require("./controllers/" + file);
        route.controller(app, _);
    }
});

app.use(history());
app.use(serveStatic(__dirname + "/dist"));

router.get('/api/current_user', isLoggedIn, function(req, res) {
    if(req.user) {
        res.send({ current_user: req.user })
    } else {
        res.status(403).send({ success: false, msg: 'Unauthorized.' });
    }
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
    console.log('error! auth failed')
};

router.get('/api/logout', function(req, res){
    req.logout();
    res.send();
});   

router.get('/', function (req, res) {
    res.json({message: 'API Initialized'});
});

const port = process.env.API_PORT || 8081;

app.use('/', router);

app.listen(port, function () {
    console.log(`api running on port ${port}`);
});